'use strict'

const express = require('express');
const app = express();
const db = require('./conf/db');
const port = process.env.PORT || 3000;
const env =  process.env.NODE_ENV || 'development';

app.set('json spaces',3);

app.get('/', async(req,res,next)=>{
	let OrderDetail=[];
	try{
		const kendaraan = await db.any('select * from ms_kendaraan order by max_weight asc');
		const service = await db.any('select * from ms_service');
		const tr_order = await db.any('select * from tr_order');
		for (var i = tr_order.length - 1; i >= 0; i--) {
			let order = tr_order[i];
			await getSrv(order.id_order).then(service=>{
				OrderDetail.push(Object.assign(order, {services:service}))	
			} );

			await getRoute(order.id_order).then(route=>{
				Object.assign(order, {routes:route})	
			} );
		}
		
		res.json({kendaraan:kendaraan, services:service, orders:OrderDetail});
	}catch(e){
		if(env==='development')console.log(e)
		res.json({success:false});		
	}
});

const getSrv = (id_order) =>{
	return new Promise((resolve, reject) =>{
		db.any(`select a.id_ord_service, a.id_service, b.nm_service, b.detail_service from tr_order_services a left join ms_service b on a.id_service=b.id_service where a.id_order=${id_order}`)
			.then(service =>{
				resolve(service)
			})
			.catch(err => {
				reject(err)
			});
	})
}

const getRoute = (id_order) =>{
	return new Promise((resolve, reject) =>{
		db.any(`select id_ord_route, alamat from tr_order_routes where id_order=${id_order} order by jarak asc`)
			.then(route =>{
				resolve(route)
			})
			.catch(err => {
				reject(err)
			});
	})
}


app.use((req,res) =>{
	res.status(404).json({success:false});
})

app.listen(port, console.log(`server ${env} started at ${port}`));
